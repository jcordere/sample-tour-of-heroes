# Sample Tour of Heros

Aqui se encuentra distintos proyectos en Angular, los cuales sirven para explicar distintos conceptos necesarios para el desarrollo de aplicaciones.

## Getting Started

Al descargar los proyectos se puede entrar en cada uno y ejecutarlos, ademas se incluye un ejemplo de mockservice para SoapUI el cual se utiliza para explicar la implementación de llamadas a Web Service REST reales.

### Prerequisites

Es necesario tener instalado

```
Git
Node JS
NPM
Angular CLI
```
